// 定义皮肤数据 仓库

import { defineStore } from 'pinia'
import { getinfo } from '@/utils/api'

// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
const useSkinStore = defineStore('skin', {
    state() {
        return {
            // 所有皮肤，供用户切换皮肤
            skins: null,
            // 当前用户皮肤
            currentSkin: '',
            // 皮肤弹框是否显示
            dialogVisible: false,
            // 切换皮肤，加载状态
            changeSkinLoading: false,
        }
    },
    actions: {
        // todo 获取所有皮肤
        async getAllSkins() {
            const { skins } = await getinfo(`skins`)
            // console.log(skins)
            this.skins = skins
        },
        // todo 切换当前皮肤
        changeCurrentSkin(skin) {
            this.currentSkin = skin
        },
        // 设置皮肤弹框是否显示
        changeDialogVisible(isShow) {
            this.dialogVisible = isShow
        },
    },
    getters: {},
})

export default useSkinStore
