import { defineStore } from 'pinia'
import { getinfo } from '@/utils/api'

// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
const useAvatarStore = defineStore('avatar', {
    state() {
        return {
            // 所有头像，可供用户选择
            avatars: null,
            // 登录时候的头像
            loginAvatar: null,
        }
    },
    actions: {
        // 获取所有头像
        async getAllAvatars() {
            const { avatars } = await getinfo(`avatars`)
            this.avatars = avatars
        },
        // 获取登录时候的头像
        async getLoginAvatar() {
            const { avatar } = await getinfo(`avatar`)
            this.loginAvatar = avatar
        },
    },
})

export default useAvatarStore
