import { defineStore } from 'pinia'

const useTeacherStore = defineStore('teacher', {
    state() {
        return {
            // 是否是老师
            isTeacher: false,
        }
    },
    actions: {
        teacherHandler() {
            // 是老师
            this.isTeacher = true
        },
    },
})

export default useTeacherStore
