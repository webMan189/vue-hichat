import { defineStore } from 'pinia'

// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
const useMentionStore = defineStore('mention', {
    state() {
        return {
            // @ 人
            mentionPersonId: null,
        }
    },
    actions: {
        setMentionPersonId(id) {
            this.mentionPersonId = id
        },
    },
})

export default useMentionStore
