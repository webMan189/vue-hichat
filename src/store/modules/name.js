import { defineStore } from 'pinia'
import { getinfo } from '@/utils/api'
import useTeacherStore from './teacher'

// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
const useNameStore = defineStore('name', {
    state() {
        return {
            // 所有班级学员姓名，可供随机点名
            names: null,
            // 昵称
            nickname: null,
        }
    },
    actions: {
        // 获取所有班级学员姓名
        async getAllNames() {
            const teacher = useTeacherStore()
            const { names } = await getinfo(`names`, teacher.isTeacher)
            this.names = names
        },
        // 获取昵称
        async getNickName() {
            const { nickname } = await getinfo(`nickname`)
            this.nickname = nickname
        },
        // 随机点名，把抽取到的人，发给服务器记录次数
        addCount(id) {
            // console.log(id)
            const obj = this.names.find((item) => item.id === id)
            obj.count++
        },
    },
})

export default useNameStore
