import { defineStore } from 'pinia'

// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
const useRadioStore = defineStore('radioMsg', {
    state() {
        return {
            // 广播消息
            radioInfo: {
                // 装所有的数据
                list: [],
                newMsg: 0,
            },
        }
    },
    actions: {
        // 添加广播消息
        addRadio(data) {
            // 添加数据
            this.radioInfo.list.push(data)
            this.radioInfo.newMsg++
        },
        resetRadio() {
            // 鼠标移入上去，重置掉新的未读消息
            this.radioInfo.newMsg = 0
        },
    },
})

export default useRadioStore
