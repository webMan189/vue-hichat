import { defineStore } from 'pinia'

// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
const useLoginStore = defineStore('login', {
    state() {
        return {
            // 显示登录还是首页 true登录 、false首页
            showLogin: true,
        }
    },
    actions: {
        // 切换登录，首页
        showLoginFn(flag = true) {
            this.showLogin = flag
        },
    },
})

export default useLoginStore
