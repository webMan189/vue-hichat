import { defineStore } from 'pinia'

// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
const useUsersStore = defineStore('users', {
    state() {
        return {
            // 所有用户列表
            users: [],
        }
    },
    actions: {
        // 添加用户
        addUser(user) {
            this.users.push(user)
        },
    },
    getters: {
        // 计算点击选择的用户（用于私聊）
        selectedUser(user) {
            if (user) {
                return user
            }
            // 没有user数据，则默认选中的就是第一个
            return this.users[0]
        },
        // 计算当前登录的用户自己
        selfUser() {
            return this.users.find((user) => user.self)
        },
    },
})

export default useUsersStore
