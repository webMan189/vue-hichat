import useUsersStore from './modules/users'
import useSkinStore from './modules/skin'
import useTeacherStore from './modules/teacher'
import useRadioStore from './modules/radioMsg'
import useLoginStore from './modules/login'
import useMentionStore from './modules/mention'
import useAvatarStore from './modules/avatar'
import useNameStore from './modules/name'

// 函数内返回对象，里面有仓库模块
const useStore = () => {
    return {
        // 登录
        login: useLoginStore(),
        // 头像
        avatar: useAvatarStore(),
        // 用户列表
        users: useUsersStore(),
        // 点名的学生名 + 随机的昵称
        name: useNameStore(),
        // 皮肤
        skin: useSkinStore(),
        // 老师身份
        teacher: useTeacherStore(),
        // 广播
        radio: useRadioStore(),
        // @ 提及
        mention: useMentionStore(),
    }
}

// 默认导出函数
export default useStore
