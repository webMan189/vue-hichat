// pinia 中存储的数据
//  1. users 所有用户数据（包括用户对应的聊天数据）
//              需要从中计算出 当前用户的数据，计算出当前用户是否为老师
//  2. 点名数据
//  3. 歌曲数据
//  4. 广播数据
//  5. 皮肤（壁纸）数据

import { defineStore } from 'pinia'
// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
export const useStore = defineStore('user', {
    state() {
        return {}
    },
    actions: {},
})
