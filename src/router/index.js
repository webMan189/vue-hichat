import Vue from 'vue'
import VueRouter from 'vue-router'
// import store from '@/store'

// 导入一级组件
import UserLogin from '@/views/UserLogin'
import Index from '@/views/index'
import NotFound from '@/components/NotFound'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: { name: 'login' },
    },
    {
        path: '/index',
        name: 'index',
        component: Index,
    },
    {
        path: '/login',
        name: 'login',
        component: UserLogin,
    },
    {
        path: '*',
        component: NotFound,
    },
]

const router = new VueRouter({
    routes,
})

// 全局的导航守卫 ==> 目标（用户没有登录，就不能访问/user这些页面）
// 步骤
// 1. 判断是否有token
// 2. 如果有token 直接放行
// 3. 如果没有token，在判断去哪
// 3.1 /home /video 等页面  直接放行
// 3.2 /user 编辑资料等页面  拦截去登录

// 需要拦截的页面(需要登录才可以访问到)

export default router
