// ! bug列表
// 更好的写法： https://juejin.cn/post/6844903480696504328

export default function showCanvas(ctx, { x, y, flag }) {
    ctx.canvas.style.display = 'block'
    ctx.canvas.width = window.innerWidth //设置画布大小
    ctx.canvas.height = window.innerHeight
    var content2d = ctx.canvas.getContext('2d') //转化为2d模式

    var b = new ball()
    b.init(-10000, -10000)
    var balls = [b] //建立数组用于存放小球

    function ball() {
        //构造函数方法开发
        this.color = null //分别为小球的颜色，x y坐标，半径，角度，角度移动x，y坐标
        this.x = null
        this.y = null
        this.r = null
        this.Angle = null
        this.Anglex = null
        this.Angley = null
        this.init = function (x, y) {
            // 初始化状态
            this.x = x
            this.y = y
            this.r = this.randomNum(15, 25)
            this.color = this.randomColor()
            this.Angle = Math.random() * Math.PI * 2
            this.Anglex = this.randomNum(10, 12) * Math.cos(this.Angle) //利用三角函数来控制小球的移动方向
            this.Angley = this.randomNum(10, 12) * Math.sin(this.Angle)
        }
        this.randomColor = function () {
            //随机颜色
            return '#' + parseInt(Math.random() * 16777216).toString(16)
        }
        this.randomNum = function (max, min) {
            //随机坐标**/*-*-+**
            return Math.random() * max + min
        }
        this.move = function () {
            //利用坐标的偏移和半径的减少和重绘来实现小球的移动
            this.x += this.Anglex * 1.1
            this.y += this.Angley * 1.1
            this.r -= 0.35
            this.Anglex *= 0.92
            this.Angley *= 0.92
        }
    }

    function createBall(x, y) {
        var count = parseInt(Math.random() * 15 + 15) //随机小球数量
        for (var i = 0; i < count; i++) {
            var b = new ball()
            b.init(x, y)
            balls.push(b)
        }
    }

    function Draw() {
        for (var i = 0; i < balls.length; i++) {
            var b = balls[i]
            b.move()
            content2d.beginPath()
            content2d.fillStyle = b.color
            content2d.arc(b.x, b.y, b.r, 0, Math.PI * 2)
            content2d.fill()
            content2d.closePath()
            // console.log(1);
        }
    }

    function removeBall() {
        //如果小球的半径小于等值，移除小球
        for (var i = 0; i < balls.length; i++) {
            var b = balls[i]
            if (b.r < 0.35) {
                balls.splice(i, 1)
                i--
            }
        }
    }

    let timer = null
    loop()
    function loop() {
        //（重绘）h5 计时器，强大好用
        // 清除整个canvas
        content2d.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
        Draw()
        removeBall()
        // console.log(1, balls.length)
        timer = window.requestAnimationFrame(loop)
        // console.log(flag)
        if (balls.length === 0 || !flag) {
            // 以取消回调函数
            window.cancelAnimationFrame(timer)
            ctx.canvas.style.display = 'none'
        }
    }

    createBall(x, y)
}
