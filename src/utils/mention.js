export function MentionCustomization(editor) {
    // The upcast converter will convert <a class="mention" href="" data-user-id="">
    // elements to the model 'mention' attribute.
    editor.conversion.for('upcast').elementToAttribute({
        view: {
            name: 'span',
            key: 'data-mention',
            classes: 'mention',
            attributes: {
                'data-user-id': true,
            },
        },
        model: {
            key: 'mention',
            value: (viewItem) => {
                // The mention feature expects that the mention attribute value
                // in the model is a plain object with a set of additional attributes.
                // In order to create a proper object, use the toMentionAttribute helper method:
                const mentionAttribute = editor.plugins
                    .get('Mention')
                    .toMentionAttribute(viewItem, {
                        // Add any other properties that you need.
                        userID: viewItem.getAttribute('data-user-id'),
                    })

                return mentionAttribute
            },
        },
        converterPriority: 'high',
    })

    // Downcast the model 'mention' text attribute to a view <a> element.
    editor.conversion.for('downcast').attributeToElement({
        model: 'mention',
        view: (modelAttributeValue, { writer }) => {
            // Do not convert empty attributes (lack of value means no mention).
            if (!modelAttributeValue) {
                return
            }

            return writer.createAttributeElement(
                'span',
                {
                    class: 'mention',
                    'data-mention': modelAttributeValue.id,
                    'data-user-id': modelAttributeValue.userID,
                },
                {
                    // Make mention attribute to be wrapped by other attribute elements.
                    priority: 20,
                    // Prevent merging mentions together.
                    id: modelAttributeValue.uid,
                }
            )
        },
        converterPriority: 'high',
    })
}

export function getFeedItems(items, queryText) {
    // As an example of an asynchronous action, return a promise
    // that resolves after a 100ms timeout.
    // This can be a server request or any sort of delayed action.
    return new Promise((resolve) => {
        setTimeout(() => {
            const itemsToDisplay = items
                // Filter out the full list of all items to only those matching the query text.
                .filter(isItemMatching)
            // Return 10 items max - needed for generic queries when the list may contain hundreds of elements.
            // .slice(0, 10)  // 去掉不限制显示数量

            resolve(itemsToDisplay)
        }, 100)
    })

    // Filtering function - it uses `name` and `username` properties of an item to find a match.
    function isItemMatching(item) {
        // Make the search case-insensitive.
        // const searchString = queryText.toLowerCase()
        const searchString = queryText ? queryText.toLowerCase() : queryText

        // Include an item in the search results if name or username includes the current user input.
        return (
            item.name.toLowerCase().includes(searchString) ||
            item.id.toLowerCase().includes(searchString)
        )
    }
}

export function customItemRenderer(item) {
    const itemElement = document.createElement('span')

    itemElement.classList.add('custom-item')
    itemElement.id = `mention-list-item-id-${item.userID}`
    itemElement.textContent = `${item.name} `

    const usernameElement = document.createElement('span')

    usernameElement.classList.add('custom-item-username')
    usernameElement.textContent = item.id

    itemElement.appendChild(usernameElement)

    return itemElement
}
