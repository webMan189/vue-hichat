import confetti from 'canvas-confetti'
import _ from 'lodash'

// 方式：五彩纸屑的展示（我给别人点赞）  这个有些bug，有时候效果出不来
export const confettiShow = (myCanvas) => {
    const myConfetti = confetti.create(myCanvas, {
        resize: true,
        useWorker: true,
    })
    myConfetti({
        angle: _.random(55, 125),
        spread: _.random(50, 70),
        particleCount: _.random(50, 200),
        origin: { y: 0.6 },
        // any other options from the global
        // confetti function
    })
}

// 方式3：别人给我点赞（Realistic Look）
export const confettiToMe3 = () => {
    const count = 200
    const defaults = {
        origin: { y: 0.7 },
        angle: _.random(55, 125),
        spread: _.random(50, 70),
    }

    function fire(particleRatio, opts) {
        confetti(
            Object.assign({}, defaults, opts, {
                particleCount: Math.floor(count * particleRatio),
            })
        )
    }

    fire(0.25, {
        spread: 26,
        startVelocity: 55,
    })
    fire(0.2, {
        spread: 60,
    })
    fire(0.35, {
        spread: 100,
        decay: 0.91,
        scalar: 0.8,
    })
    fire(0.1, {
        spread: 120,
        startVelocity: 25,
        decay: 0.92,
        scalar: 1.2,
    })
    fire(0.1, {
        spread: 120,
        startVelocity: 45,
    })
}

// 方式2：别人给我点赞 (左右 2边 纸屑)
export const confettiToMe2 = () => {
    const end = Date.now() + 5 * 1000

    // go Buckeyes!
    const colors = ['#bb0000', '#ffffff']
    let frameID
    ;(function frame() {
        confetti({
            particleCount: 2,
            angle: 60,
            spread: 55,
            origin: { x: 0 },
            colors: colors,
        })
        confetti({
            particleCount: 2,
            angle: 120,
            spread: 55,
            origin: { x: 1 },
            colors: colors,
        })

        if (Date.now() < end) {
            frameID = requestAnimationFrame(frame)
        } else {
            cancelAnimationFrame(frameID)
        }
    })()
}

// 方式1：别人给我点赞（Fireworks）
export const confettiToMe = () => {
    const duration = 5 * 1000
    const animationEnd = Date.now() + duration
    const defaults = {
        startVelocity: 30,
        spread: 360,
        ticks: 60,
        zIndex: 0,
    }

    let interval = setInterval(function () {
        var timeLeft = animationEnd - Date.now()

        if (timeLeft <= 0) {
            return clearInterval(interval)
        }

        let particleCount = 50 * (timeLeft / duration)
        // since particles fall down, start a bit higher than random
        confetti(
            Object.assign({}, defaults, {
                particleCount,
                origin: {
                    x: _.random(0.1, 0.3),
                    y: Math.random() - 0.2,
                },
            })
        )
        confetti(
            Object.assign({}, defaults, {
                particleCount,
                origin: {
                    x: _.random(0.7, 0.9),
                    y: Math.random() - 0.2,
                },
            })
        )
    }, 250)
}
