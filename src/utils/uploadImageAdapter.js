import socket from './socket'

export default class uploadImageAdapter {
    constructor(loader) {
        this.loader = loader
    }

    async upload() {
        // 获取到用户上传的文件
        const image = await this.loader.file

        // const formData = new FormData()
        // formData.append('file', image)
        // console.log(image)

        const isImage = image.type.includes('image')
        const isLt5M = image.size / 1024 / 1024 <= 10

        //  isImage && isLt5M
        // if (!isImage) this.$message.error('只能上传图片!')
        // if (!isLt5M) this.$message.error('上传头像图片大小不能超过 10MB!')

        const data = await this.customUpload(image)
        //返回的结果格式化一下,把url传到下面的json中
        // console.log(data)
        const res = {
            uploaded: true,
            default: data.url,
        }
        // console.log(res)
        // 必须返回对象，其中有 uploaded 和 default属性（是上传后的图片地址用于在编辑器中展示的图片地址）
        return res
    }

    customUpload(file) {
        // 自定义上传文件 实现上传逻辑
        return new Promise((resolve) => {
            socket.emit('upload', file, (data) => {
                // console.log(data)
                resolve(data)
            })
        })
    }

    abort() {}
}

function MyCustomUploadAdapterPlugin(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
        // 在这里将URL配置为后端上载脚本
        return new uploadImageAdapter(loader)
    }
}

export { uploadImageAdapter, MyCustomUploadAdapterPlugin }

/* 编辑器配置 */
const editorConfig = {
    // placeholder: '让我们嗨聊起来, Ctrl + Enter 换行哦！',
    placeholder: '让我们嗨聊起来',
    MENU_CONF: {
        uploadImage: {
            // 服务端地址
            // 单个文件的最大体积限制，默认为 2M
            maxFileSize: 10 * 1024 * 1024, // 10M
            // 最多可上传几个文件，默认为 100
            maxNumberOfFiles: 10,
            // 选择文件时的类型限制，默认为 ['image/*'] 。如不想限制，则设置为 allowedFileTypes: [],
            allowedFileTypes: ['image/*'],
            // allowedFileTypes: [],
            // 跨域是否传递 cookie ，默认为 false
            withCredentials: true,
            // 超时时间，默认为 10 秒
            //   timeout: 5 * 1000, // 5 秒
            customUpload(file, insertFn) {
                // 自定义上传文件
                // console.log('customUpload', file)

                return new Promise((resolve) => {
                    this.$socket.emit('upload', file, (data) => {
                        // console.log(data)
                        insertFn(data.url, data.alt, data.href)
                        resolve('ok')
                    })
                })
            },
        },
    },
}
