// socket.io

import { io } from 'socket.io-client'

// console.log(process.env.VUE_APP_BASE_URL) // 本地ip地址 192.168.80.1
const URL = `http://${process.env.VUE_APP_BASE_URL}:8888/`

// autoConnect is set to false so the connection is not established right away.
// We will manually call socket.connect() later, once the user has selected a username.
let socket = io(URL, {
    autoConnect: false,
})

// socket.emit('chat message', 'is me, get my message?')
// socket.on('chat message', function (msg) {
//     console.log(msg)
// })
// Vue.prototype.$socket = socket

// We also register a catch-all listener, which is very useful during development
// 我们还注册了一个通用侦听器，这在开发过程中非常有用
// socket.onAny((event, ...args) => {
//     console.log(event, args)
// })
// So that any event received by the client will be printed in the console.
// 以便客户机接收到的任何事件都将打印在控制台中。

// 导出
export default socket
