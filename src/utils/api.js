// 获取头像和皮肤数据
// import Vue from 'vue'
import axios from 'axios'
export const URL = `http://${process.env.VUE_APP_BASE_URL}:8888/`

// 获取头像 + 所有头像 + 皮肤数据
export const getinfo = async (type, isTeacher = false) => {
    // const {
    //     data: { avatar, avatars, skins },
    // } = await axios.get(`${URL}getinfo/${type}`)
    // console.log(avatar, avatars, skins)

    const { data } = await axios.get(`${URL}getinfo/${type}?tflag=${isTeacher}`)
    // console.log(data)

    return data

    // avatar && (Vue.prototype.$avatar = avatar)
    // avatars && (Vue.prototype.$avatars = avatars)
    // skins && (Vue.prototype.$skins = skins)
}
