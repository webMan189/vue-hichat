// 正则解决方案-给图片添加属性（以便可以实现图片预览功能）
export function addImageAttr(content, username, dataName = 'data-href') {
    // 第一个replace去掉 p标签包含的 &nbsp;
    // 第二个replace专门处理img，给其替换带属性的img
    return content
        .replace(/<(\w+)[^>]*>&nbsp;(.*?<\/\1>)?/, '')
        .replace(/<img (src="(\S+)")>/gm, ($1, $2, $3) => {
            return `<img src="${$3}" alt="${username}" ${dataName}="${$3}">`
        })
}

// 插入图片，决定光标的位置
export const imagePosition = (editor) => {
    editor.plugins
        .get('ImageUploadEditing')
        .on('uploadComplete', (evt, { data, imageElement }) => {
            // console.log('uploadComplete')

            // 实现上传图片之后，决定光标位置
            editor.model.change((writer) => {
                // 将光标定位到当前上传图片之后
                writer.setSelection(
                    writer.createPositionAt(imageElement, 'after')
                )

                // 将光标定位到最后
                // writer.setSelection(
                //     writer.createPositionAt(
                //         editor.model.document.getRoot(),
                //         'end'
                //     )
                // )

                // 将光标定位到最前面
                // writer.setSelection(
                //     writer.createPositionAt(editor.model.document.getRoot(), 0)
                // )
            })
        })
}

// ! 处理回车功能
export const enterHandler = (editor, cb) => {
    // ctrl + enter 换行
    editor.editing.view.document.on('keyup', (evt, data) => {
        // console.log('Pressed key: ', data.domEvent)
        if (data.domEvent.ctrlKey && data.domEvent.key === 'Enter') {
            // shift + enter creates a br tag
            editor.execute('shiftEnter')
            //cancel enter event to prevent p tag
            data.preventDefault()
            evt.stop()
        }
    })

    // enter 发送消息
    editor.editing.view.document.on('enter', (evt, data) => {
        // shift + enter creates a br tag
        // editor.execute('shiftEnter')
        //cancel enter event to prevent p tag
        data.preventDefault()
        evt.stop()

        // 执行回调函数
        cb && cb()
    })
}

// 图片添加属性（block 是ok的，inline会报错bug）
export function imageAddAttribute(editor) {
    // todo 对于 block 的图片添加属性
    // editor.plugins
    //     .get('ImageUploadEditing')
    //     .on('uploadComplete', (evt, { data, imageElement }) => {
    //         editor.model.change((writer) => {
    //             writer.setAttribute(dataName, data.default, imageElement)
    //         })
    //     })
    // editor.model.schema.extend('imageBlock', {
    //     allowAttributes: dataName,
    // })
    // editor.conversion.for('upcast').attributeToAttribute({
    //     view: dataName,
    //     model: dataName,
    // })
    // editor.conversion.for('downcast').add((dispatcher) => {
    //     dispatcher.on(
    //         `attribute:${dataName}:imageBlock`,
    //         (evt, data, conversionApi) => {
    //             if (!conversionApi.consumable.consume(data.item, evt.name)) {
    //                 return
    //             }
    //             const viewWriter = conversionApi.writer
    //             const figure = conversionApi.mapper.toViewElement(data.item)
    //             const img = figure.getChild(0)
    //             if (data.attributeNewValue !== null) {
    //                 viewWriter.setAttribute(
    //                     dataName,
    //                     data.attributeNewValue,
    //                     img
    //                 )
    //             } else {
    //                 viewWriter.removeAttribute(dataName, img)
    //             }
    //         }
    //     )
    // })
}

// 测试
const demo = () => {
    // todo @功能 插入内容
    const model = this.editor.model
    model.change((writer) => {
        // const selection = model.document.selection
        // const currentAttributes = selection.getAttributes()
        // const insertPosition = selection.focus
        // ok
        // writer.insertText(
        //     this.setMentionPerson({ userID: 111, username: '韩雪' }),
        //     currentAttributes,
        //     insertPosition
        // )
        // 也是ok的（但还是不能识别标签）
        // model.insertContent(
        //     writer.createText(
        //         this.setMentionPerson({
        //             userID: 111,
        //             username: '韩雪',
        //         }),
        //         currentAttributes
        //     )
        // )
    })

    // 获取内容
    // console.log(this.editor.getData())

    // 测试
    // console.log(this.editor)
    // this.editor.model.change((writer) => {
    //     const selection = this.editor.model.document.selection
    //     const currentAttributes = selection.getAttributes()
    //     const insertPosition = selection.focus
    //     writer.insertText('[Foo]', currentAttributes, insertPosition)
    // })
}
