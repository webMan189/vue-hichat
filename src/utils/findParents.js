// 递归找父元素
export default function findParents(el, parentSelect) {
    // console.log(el)

    // todo 判断el元素是否是parentSelect的父级元素 (实现有难度)
    // if(el.contains()){}

    // 如果查找的元素就是当前需要查找的父元素，直接返回el
    if (el.matches(parentSelect)) return el

    let parent = el.parentElement
    // console.log(parent)
    if (!parent) return false

    if (!parent.matches(parentSelect)) {
        parent = findParents(parent, parentSelect)
    }

    return parent
}
