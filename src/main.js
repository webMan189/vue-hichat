/*
 * @Author: web158 1587028950@qq.com
 * @Date: 2023-03-22 21:49:37
 * @LastEditors: web158 158@qq.com
 * @LastEditTime: 2023-03-24 01:05:25
 * @FilePath: \Code_vue-hichat\src\main.js
 * @Description: 大神聊天群，点名，点歌
 */

import Vue from 'vue'
// import router from '@/router'
import App from './App.vue'

// 富文本编辑器
import CKEditor from '@ckeditor/ckeditor5-vue2'
Vue.use(CKEditor)

import socket from './utils/socket'
Vue.prototype.$socket = socket

Vue.config.productionTip = false

// 波纹效果(不好用)
// import VueTouchRipple from 'vue-touch-ripple'
// import 'vue-touch-ripple/dist/vue-touch-ripple.css'
// // mount with global
// Vue.use(VueTouchRipple)

// 波纹效果
import Ripple from 'vue-ripple-directive'
Vue.directive('ripple', Ripple)

// 头像裁切
import VueCropper from 'vue-cropper'
Vue.use(VueCropper)

// ElementUI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 外提供了一系列类名，用于在某些条件下隐藏元素
import 'element-ui/lib/theme-chalk/display.css'
Vue.use(ElementUI)

// cssshake
// import 'csshake'

// 导入字体图标
import '@/assets/fonts/iconfont.css'

// 处理时间
import moment from 'moment'
moment.locale('zh-CN')

Vue.filter('relativeTime', (time) => {
    // 相对时间
    // return moment(time).startOf('second').fromNow()

    // 时间格式
    return moment(time).format('a HH:mm:ss')
})

// import 'viewerjs/dist/viewer.css'
// import VueViewer from 'v-viewer'
// Vue.use(VueViewer)

// pinia
import { createPinia, PiniaVuePlugin } from 'pinia'
Vue.use(PiniaVuePlugin)
const pinia = createPinia()

new Vue({
    render: (h) => h(App),
    pinia,
    // router,
}).$mount('#app')
