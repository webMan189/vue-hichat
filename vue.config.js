const { defineConfig } = require('@vue/cli-service')

// 获取以太网
let needHost = '0.0.0.0'
try {
    let network = os.networkInterfaces()
    // console.log(network)
    needHost = network[Object.keys(network)[0]][1].address
} catch {
    needHost = 'localhost'
}

let os = require('os'),
    iptable = {},
    ifaces = os.networkInterfaces()

for (var dev in ifaces) {
    ifaces[dev].forEach(function (details, alias) {
        if (details.family == 'IPv4') {
            // console.log(dev)
            iptable[dev + (alias ? ':' + alias : '')] = details.address

            if (dev.includes('以太网')) {
                console.log(`以太网: ${details.address}`)
                needHost = details.address
            }
        }
    })
}

console.log(needHost)

// 在客户端使用
process.env.VUE_APP_BASE_URL = needHost

module.exports = defineConfig({
    transpileDependencies: true,
    lintOnSave: false,
    // 开发服务器
    devServer: {
        host: needHost,
        port: 8080,
        open: true,
        proxy: {},
    },
    publicPath: './',
})
