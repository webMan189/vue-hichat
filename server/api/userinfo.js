const fs = require('fs')

// 导入配置文件
const {
    avatarSheet,
    usernameSheet,
    namesSheet,
    skinSheet,
} = require('../config')

// 导入皮肤和头像
const avatars = require(avatarSheet)
const nickname = require(usernameSheet)
const { skins } = require(skinSheet)

// 提供获取头像功能
const getUserInfo = (app) => {
    app.get('/getinfo/:type', async (req, res, next) => {
        const type = req.params.type
        const teacherFlag = req.query.tflag
        // console.log(teacherFlag, typeof teacherFlag)

        let obj = null

        if (type === 'avatar') {
            // 随机
            const index1 = parseInt(avatars[0].length * Math.random())
            const index2 = parseInt(avatars[0].length * Math.random())
            const avatar = avatars[index1][index2]

            obj = { avatar }
        } else if (type === 'avatars') {
            obj = { avatars }
        } else if (type === 'skins') {
            obj = { skins }
        } else if (type === 'names') {
            // 由于点名中包含次数，需要每次获取文件最新结果，所以读取文件内容
            let names = []
            try {
                // 读取json文件
                names = JSON.parse(fs.readFileSync(namesSheet).toString())

                if (teacherFlag === 'false') {
                    // 如果不为老师，就把数据中的敏感数据给过滤掉
                    names.forEach((item) => {
                        delete item.attention
                        delete item.level
                    })
                }
            } catch (e) {
                names = []
                // console.log(e)
            }

            obj = { names }
        } else if (type === 'nickname') {
            obj = { nickname }
        }

        res.status(200).send(obj)

        await next()
    })
}

module.exports = getUserInfo
