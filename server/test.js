function nameToStar(uname, arr) {
    const res = arr.find((item) => uname.includes(item))
    const reg = new RegExp(res, 'g')

    if (res) {
        return uname.replace(reg, () => '*'.repeat(res.length))
    }
    // 原样输出
    return uname
}

const _ = require('lodash')
const axios = require('axios')
const path = require('path')

{
    const crypto = require('crypto')
    const randomId = (num = 8) => crypto.randomBytes(num).toString('hex')
    console.log(randomId(8))
    console.log(randomId(16))
    console.log(randomId(32))
}

// {
//     console.log(
//         path.normalize(
//             'http://192.168.80.1:8888/',
//             'files',
//             '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d.jpg'
//         )
//     )
// }

{
    // const arr = [
    //     '如河',
    //     '如海',
    //     '毛天',
    //     '洪轩',
    //     '尼玛',
    //     '你爹',
    //     '你跌',
    //     '你大爷',
    //     '你妈',
    //     '你妈逼',
    //     '逼',
    // ]
    // const uname = '是毛天哈市'
    // console.log(nameToStar(uname, arr))
    // console.log(nameToStar('是新人啊', arr))
    // 思路：
    //  1. 用户输入的名字通过数组检查下，看看是否包含有敏感词
    //  2. 有的话，就通过正则 + replace 来进行替换
    // const uname = '是毛天哈市'
    // const res = arr.find((item) => uname.includes(item))
    // console.log(res) // 毛天
    // const reg = new RegExp(res, 'g')
    // if (res) {
    //     const demo = res.replace(reg, () => {
    //         return '*'.repeat(res.length)
    //     })
    //     console.log(demo)
    // }
    // console.log(arr.includes(uname))
}

{
    // const fs = require('fs')
    // const { namesSensitive } = require('./config')
    // // let res = fs.readFileSync(namesSensitive).toString()
    // // console.log(JSON.parse(res))
    // // fs.watchFile(namesSensitive, () => {
    // //     res = fs.readFileSync(namesSensitive).toString()
    // //     console.log(JSON.parse(res))
    // // })
    // const readNames = require('./utils/nicknameToStar')
    // console.log(readNames())
    // fs.watchFile(namesSensitive, () => {
    //     console.log(readNames())
    // })
}

{
    // const request = require('request')
    // request(
    //     'https://api.codelife.cc/wallpaper/random?lang=cn',
    //     function (error, response, body) {
    //         if (!error && response.statusCode == 200) {
    //             console.log(body) // 请求成功的处理逻辑，注意body是json字符串
    //         }
    //     }
    // )
}

{
    // const { v4: uuidv4 } = require('uuid')
    // // 老师验证码 ==> 登录凭证
    // const teacher_code_one = uuidv4()
    // const teacher_code_two = uuidv4()
    // // 老师map数据
    // const teacherMap = new Map()
    // teacherMap.set(teacher_code_one, { a: 1 })
    // teacherMap.set(teacher_code_two, { b: 2 })
    // console.log(teacherMap)
}

{
    // const chalk = require('chalk')
    // const log = console.log
    // log(`
    //     ${chalk.bold.black.bgBlue('老师登录验证码：')}
    //     ${chalk.blue.underline.bold(`a3e9d14d-70e6-4697-ae0f-37afb8475d5e`)}
    // `)
    // const name = 'Sindre'
    // console.log(chalk.green('Hello %s'), name)
    // //=> 'Hello Sindre'
    // const error = chalk.bold.red
    // const warning = chalk.hex('#FFA500') // Orange color
    // console.log(error('Error!'))
    // console.log(warning('Warning!'))
    // // Combine styled and normal strings
    // log(chalk.blue('Hello') + ' World' + chalk.red('!'))
    // // Compose multiple styles using the chainable API
    // log(chalk.blue.bgRed.bold('Hello world!'))
    // // Pass in multiple arguments
    // log(chalk.blue('Hello', 'World!', 'Foo', 'bar', 'biz', 'baz'))
    // // Nest styles
    // log(chalk.red('Hello', chalk.underline.bgBlue('world') + '!'))
    // // Nest styles of the same type even (color, underline, background)
    // log(
    //     chalk.green(
    //         'I am a green line ' +
    //             chalk.blue.underline.bold('with a blue substring') +
    //             ' that becomes green again!'
    //     )
    // )
    // // ES2015 template literal
    // log(`
    // CPU: ${chalk.red('90%')}
    // RAM: ${chalk.green('40%')}
    // DISK: ${chalk.yellow('70%')}
    // `)
    // // Use RGB colors in terminal emulators that support it.
    // log(chalk.rgb(123, 45, 67).underline('Underlined reddish color'))
    // log(chalk.hex('#DEADED').bold('Bold gray!'))
}
