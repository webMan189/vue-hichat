const path = require('path')
const { v4: uuidv4 } = require('uuid')
const OSUtil = require('./utils/address')

// 配置文件
const config = {
    // 协议
    PROTOCOL: 'http',
    // ipv4
    ipv4: OSUtil.getYitai(),
    // 端口号
    port: process.env.PORT || 8888,

    // 老师验证码 ==> 登录凭证
    teacher_code: uuidv4(),

    // 静态资源（皮肤、头像）的文件夹
    ASSETS_FOLDER: 'assets',

    // upload 文件夹
    UPLOAD_FOLDER: 'upload',

    // 上传文件的文件夹
    FILES_FOLDER: 'files',

    // 上传头像的文件夹
    FILE_AVATAR_FOLDER: 'avatars',

    // 上传皮肤的文件夹
    FILE_SKIN_FOLDER: 'skins',

    // socket 服务器,限制上传文件大小
    maxHttpBufferSize: 10 * 1024 * 1024, // 10 MB

    // 学生点名表
    // 测试数据
    // namesSheet: './data/hf-names.json',
    // 29期数据
    namesSheet: path.join(__dirname, 'data/data-hf-29.json'),

    // 音乐数据
    musicSheet: path.join(__dirname, 'data/music.json'),

    // 昵称敏感词
    namesSensitive: path.join(__dirname, 'namesSensitive.json'),

    // 随机的用户名
    usernameSheet: path.join(__dirname, 'data/nickname.json'),

    // 随机的头像
    avatarSheet: path.join(__dirname, './utils/avatars.js'),

    // 随机的皮肤(九宫格数据)
    skinSheet: path.join(__dirname, './utils/skins.js'),

    // 随机的皮肤（在线接口地址）
    skinRequestUrl: {
        // 随机
        random: 'https://api.codelife.cc/wallpaper/random?lang=cn',
        // 热门 需要参数 page指定页码
        top: 'https://api.codelife.cc/wallpaper/wallhaven?lang=cn&sorting=toplist&topRange=6M',
        // 视频壁纸
        video: 'https://api.wetab.link/api/wallpaper/video-list',
    },
}
// 服务器的baseURL
config.baseURL = `${config.PROTOCOL}://${config.ipv4}:${config.port}/`

// 静态资源的文件夹完整路径
config.ASSETS_FOLDER_PATH = path.resolve(__dirname, config.ASSETS_FOLDER)
// upload文件夹完整路径
config.UPLOAD_FOLDER_PATH = path.resolve(__dirname, config.UPLOAD_FOLDER)

// 上传文件的文件夹完整路径
config.FILE_FOLDER_PATH = path.resolve(
    __dirname,
    config.UPLOAD_FOLDER,
    config.FILES_FOLDER
)

// 上传头像的文件夹完整路径
config.FILE_AVATAR_FOLDER_PATH = path.resolve(
    __dirname,
    config.UPLOAD_FOLDER,
    config.FILE_AVATAR_FOLDER
)

// 上传皮肤的文件夹完整路径
config.FILE_SKIN_FOLDER_PATH = path.resolve(
    __dirname,
    config.UPLOAD_FOLDER,
    config.FILE_SKIN_FOLDER
)

module.exports = config
