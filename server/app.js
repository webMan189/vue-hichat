const fs = require('fs')
const cors = require('cors')
const siofu = require('socketio-file-upload')
const express = require('express')
const chalk = require('chalk')
const log = console.log
const app = express().use(siofu.router)
const http = require('http').Server(app)
app.use(cors())

// todo 导入配置文件
const {
    skinSheet,
    maxHttpBufferSize,
    namesSensitive,
    UPLOAD_FOLDER_PATH,
    ASSETS_FOLDER_PATH,
    port,
    baseURL,
    teacher_code, // 老师验证码 ==> 登录凭证
} = require('./config')

// todo 导入所有皮肤
const { allSkins } = require(skinSheet)
const ioConnection = require('./utils/socketHandler/connection')
const ioAllow = require('./utils/socketHandler/ioAllowMiddleware')
const getUserInfo = require('./api/userinfo')
const { readNames } = require('./utils/nicknameToStar')
let allSenNames = readNames()
// 如果中途添加新的敏感词，监视文件的变化
fs.watchFile(namesSensitive, () => {
    allSenNames = readNames()
    // console.log('监视到文件变化, 新的数据', allSenNames)
})

// todo 提供接口（头像-皮肤-昵称）
getUserInfo(app)

// todo 托管静态资源
app.use(express.static(ASSETS_FOLDER_PATH))
app.use(express.static(UPLOAD_FOLDER_PATH))

// todo 存储用户id
const { InMemorySessionStore } = require('./utils/sessionStore')
const sessionStore = new InMemorySessionStore()

// todo 存储消息
const { InMemoryMessageStore } = require('./utils/messageStore')
const messageStore = new InMemoryMessageStore()
// todo 所有群聊消息
const chatMessageStore = []
// todo 所有用户数据
let users = []

// todo 启动 socket 服务器
// ! 通知现有用户（除了当前用户自己）
// socket.broadcast.emit("user connected", ...)是通知现有用户（除了当前用户自己）
// 广播的另一种形式是io.emit("user connected", ...)会将"user connected"事件发送给所有已连接的客户端，包括新用户。
const io = require('socket.io')(http, {
    // 限制上传文件大小
    maxHttpBufferSize,
    // 解决跨域问题
    cors: {
        origin: '*',
    },
})

// todo io的中间件，决定用户是否可以登录
ioAllow({
    io,
    sessionStore,
    users,
    allSkins,
    allSenNames,
})

// todo 用户链接 socketio
ioConnection({
    io,
    sessionStore,
    users,
    messageStore,
    chatMessageStore,
    allSkins,
    allSenNames,
})

http.listen(port, () => {
    // log(`Socket.IO server running at start ${baseURL}`)
    log(
        chalk.blueBright.underline.bold(
            `Socket.IO server running at start ${baseURL}`
        )
    )

    log(
        `
${chalk.bold.red('老师登录验证码：')}
${chalk.blue.underline.bold(teacher_code)}
`
    )
})
