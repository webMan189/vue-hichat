// ! 用于重置歌曲统计的数量

const path = require('path')
const fs = require('fs')
// const { v4: uuidv4 } = require('uuid')

// 导入配置文件，获取音乐文件
const { musicSheet } = require('./config')
// 音乐文件路径
const musicUrl = path.resolve(__dirname, musicSheet)

// 处理结果（把每一首音乐的点赞数量为100）
const musics = require(musicSheet)

// 结果写入到文件中
// fs.writeFileSync(musicUrl, JSON.stringify())
