// ! 用于给指定学生姓名文件初始化添加id唯一标识

const path = require('path')
const fs = require('fs')
const { v4: uuidv4 } = require('uuid')

// 导入配置文件，获取当前学生文件路径
const { namesSheet } = require('./config')

// 学生姓名json文件地址
const namesUrl = path.resolve(__dirname, namesSheet)

// 学生名字
const names = require(namesSheet)

// 添加id
names.forEach((item) => (item.id = uuidv4()))
// console.log(names)

// 结果写入到文件中
fs.writeFileSync(namesUrl, JSON.stringify(names))
