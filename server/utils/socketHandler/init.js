// 初始化有人进入聊天室的socket处理
const initComeIn = require('./handler/comeIn')
// 初始化离线的socket处理
const initDisconnect = require('./handler/disconnect')
// 初始化群聊的socket处理
const initChatMessage = require('./handler/chatMessage')
// 初始化私聊的socket处理
const initPrivateMessage = require('./handler/privateMessage')

// 初始化音乐的socket处理
const initMusic = require('./handler/music')
// 初始化点名的socket处理
const initCallNames = require('./handler/callnames')
// 初始化上传文件的socket处理
const initUploadFiles = require('./handler/uploadFiles')
// 初始化上传头像和皮肤的socket处理
const initUploadAvatarAndSkin = require('./handler/uploadAvatar&Skin')
// 初始化老师重置学生信息的socket处理
const initTeacherResetUser = require('./handler/teacherResetUser')
// 初始化更新头像、昵称、皮肤的socket处理
const initResetUserInfo = require('./handler/resetUserInfo')
// 初始化老师禁言的socket处理
const initTeacherDisableSendMsg = require('./handler/teacherDisableSendMsg')
// 初始化老师踢出学生的socket处理
const initTeacherGameover = require('./handler/teacherGameover')
// 初始化重置群聊消息的socket处理
const initResetChatMessage = require('./handler/resetChatMessage')
// 初始化发送炸弹、鲜花等的socket处理
const initPrivateInteraction = require('./handler/privateInteraction')

const arr = [
    initComeIn,
    initDisconnect,
    initChatMessage,
    initPrivateMessage,

    initMusic,
    initCallNames,
    initUploadFiles,
    initUploadAvatarAndSkin,
    initTeacherResetUser,
    initResetUserInfo,
    initTeacherDisableSendMsg,
    initTeacherGameover,
    initResetChatMessage,
    initPrivateInteraction,
]

const init = (obj) => {
    // 遍历数组，处理每个socket的监听
    arr.forEach((item) => item(obj))
}

module.exports = init
