const _ = require('lodash')
const crypto = require('crypto')

// 处理时间
//  1. 登录的时间
//  2. 发布消息的时间（群聊 + 私聊） moment().format()
const moment = require('moment')

const randomId = (num = 8) => crypto.randomBytes(num).toString('hex')

// 导入配置文件
const { FILE_AVATAR_FOLDER, baseURL, teacher_code } = require('../../config')

// 昵称交给nameToStar函数去处理，如果包含敏感字眼，处理成**
const { nameToStar } = require('../nicknameToStar')

// ! 注册一个中间件，它检查用户名并允许连接
const ioAllow = ({ io, sessionStore, users, allSkins, allSenNames }) => {
    // 老师 userID
    const teacher_userID = randomId(32)
    // 老师 sessionID
    const teacher_sessionID = randomId(32)
    // 老师对象信息
    const teacherInfo = {
        sessionID: teacher_sessionID,
        userID: teacher_userID,
        username: '黑马老师',
        avatar: `${baseURL}${FILE_AVATAR_FOLDER}/teacher.png`,
        skin: _.sample(allSkins),
        // 老师默认未链接
        connected: false,
        // 老师身份
        isTeacher: true,
        disabledChatMsg: false,
        time: moment().format(),
        // 私聊消息
        messages: [],
    }

    // ! 注册一个中间件，它检查用户名并允许连接
    io.use((socket, next) => {
        // console.log('所有用户 ', sessionStore.findAllSessions())

        const sessionID = socket.handshake.auth.sessionID
        if (sessionID) {
            // 找当前用户信息
            const session = sessionStore.findSession(sessionID)

            if (session) {
                // 找到了存储的用户

                // 但如果是老师，老师已经在线的话，就提示
                if (session.isTeacher && session.connected) {
                    // 老师在线中 ==> 老师账号已经登录过
                    console.log('1 ----------- 老师账号已经登录过')
                    return next(new Error('teacher is login'))
                }

                socket.sessionID = sessionID
                socket.userID = session.userID
                socket.username = session.username
                socket.avatar = session.avatar
                socket.skin = session.skin
                socket.isTeacher = session.isTeacher
                // 禁言标识（用户是否禁止群聊）
                socket.disabledChatMsg = session.disabledChatMsg
                socket.time = moment().format()
                return next()
            }
        }

        // ! 新进的用户
        // 获取用户名
        let username = socket.handshake.auth.username
        const avatar = socket.handshake.auth.avatar
        // 老师登录使用的验证码
        const code = socket.handshake.auth.code

        if (!username) {
            // 重新连接的用户，没有用户名，表示链接失败
            return next(new Error('invalid connect'))
        }

        // 判断用户名是否已被使用
        let res = users.find((item) => item.username === username)
        if (res) {
            // 用户名相同
            return next(new Error('duplication username'))
        }

        // 判断是老师还是学生
        //  如果是老师，信息从 teacherInfo 中获取，并且是登录状态
        //  是学生的话，执行默认信息
        if (username.toUpperCase() === 'ADMIN') {
            // 只有当用户名为admin的时候才判断
            // 继续判断验证码
            if (code !== teacher_code) {
                // 不是老师身份
                return next(new Error('not teacher'))
            } else {
                // 验证码正确，是老师

                // 分情况
                // 1. 老师第一次登录
                // 2. 老师掉线重新登录

                // 查找老师信息
                const teacherObj = sessionStore
                    .findAllSessions()
                    .find((item) => item.userID === teacher_userID)

                if (!teacherObj) {
                    // 还没有老师登录过，这是老师第一次登录
                    socket.sessionID = teacherInfo.sessionID
                    socket.userID = teacherInfo.userID
                    socket.username = teacherInfo.username
                    socket.avatar = teacherInfo.avatar
                    socket.skin = teacherInfo.skin
                    socket.isTeacher = teacherInfo.isTeacher
                    socket.disabledChatMsg = false
                    socket.time = moment().format()
                } else {
                    // 是老师身份
                    if (!teacherObj.connected) {
                        // teacherObj 存在，表示老师登录过了，但老师掉线了，正在重新登录
                        // ! 更新之前老师的账号为最新结果
                        socket.sessionID = teacherInfo.sessionID
                        socket.userID = teacherInfo.userID
                        socket.username = teacherInfo.username
                        socket.avatar = teacherInfo.avatar
                        socket.skin = teacherInfo.skin
                        socket.isTeacher = teacherInfo.isTeacher
                        socket.disabledChatMsg = false
                        socket.time = moment().format()
                    } else {
                        // 老师在线中 ==> 老师账号已经登录过
                        console.log('2 ----------- 老师账号已经登录过')
                        return next(new Error('teacher is login'))
                    }
                }

                next()
            }
        } else {
            // 学生处理
            socket.sessionID = randomId(16)
            socket.userID = randomId(16)
            // 昵称交给nameToStar函数去处理，如果包含敏感字眼，处理成**
            socket.username = nameToStar(username, allSenNames)
            socket.avatar = avatar
            // 随机skin作为默认图片
            socket.skin = _.sample(allSkins)
            // 默认不禁言
            socket.disabledChatMsg = false
            socket.time = moment().format()
            socket.isTeacher = false
            next()
        }
    })
}

module.exports = ioAllow
