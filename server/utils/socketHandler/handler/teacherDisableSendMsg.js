const moment = require('moment')

const initTeacherDisableSendMsg = ({ io, socket, sessionStore }) => {
    // 老师禁言学生 发表群聊
    socket.on('disableSendMsg', ({ id }, callback) => {
        // console.log('老师禁言学生 发表群聊', id)

        // 如果不是老师的操作，则阻止
        if (!socket.isTeacher) return

        // 根据id查找指定用户
        const { sessionID, user } = sessionStore.findKey(id)
        // 是否禁言，是要以session中的数据为准来做处理的
        // 修改user对象中的 disabledChatMsg为自身取反的结果
        const disabled = (user.disabledChatMsg = !user.disabledChatMsg)

        // console.log('-------------------')
        // console.log(io.of('/').sockets)

        // 更新该用户的socket中的disabledChatMsg信息
        for (let [, value] of io.of('/').sockets.entries()) {
            if (value.userID === id) {
                value.disabledChatMsg = disabled

                // 找到了该用户并修改了信息，就可以退出循环了
                break
            }
        }

        // 重新存储到session中
        sessionStore.saveSession(sessionID, {
            ...user,
            time: moment().format(),
        })

        // 群发给其他用户
        io.emit('teacherSayDisableSendMsg', { id, disabled })

        callback(disabled)
    })
}

module.exports = initTeacherDisableSendMsg
