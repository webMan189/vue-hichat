const moment = require('moment')

const initComeIn = ({ io, socket, users }) => {
    // index come in
    socket.on('index come in', () => {
        // 告知已经登录的用户
        socket.emit('users', users)
        // console.log(users)

        // 通知其他用户，新的用户登录进来
        socket.broadcast.emit('user connected', {
            userID: socket.userID,
            username: socket.username,
            avatar: socket.avatar,
            skin: socket.skin,
            isTeacher: socket.isTeacher,
            disabledChatMsg: socket.disabledChatMsg,
            connected: true,
            messages: [],
            time: moment().format(),
        })
    })
}

module.exports = initComeIn
