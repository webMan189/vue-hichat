const fs = require('fs')

// 导入配置
const { namesSheet } = require('../../../config')

const initCallNames = ({ io, socket }) => {
    // 监听点名次数记录
    socket.on('upload names count', (id, callback) => {
        // console.log('接收到的学生id', id)

        try {
            // 读取json文件
            const data = fs.readFileSync(namesSheet).toString()
            const arr = JSON.parse(data)
            const obj = arr.find((item) => item.id === id)
            obj.count++

            fs.writeFileSync(namesSheet, JSON.stringify(arr))
            callback('')
        } catch (e) {
            // 参数： 失败信息
            callback(e)
        }
    })
}

module.exports = initCallNames
