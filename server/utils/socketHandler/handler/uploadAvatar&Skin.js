const fs = require('fs')
const path = require('path')
const { v4: uuidv4 } = require('uuid')
// 解决识别base64图片后缀问题
const imageinfo = require('imageinfo')
const moment = require('moment')

// 导入配置
const {
    FILE_AVATAR_FOLDER,
    FILE_SKIN_FOLDER,
    FILE_AVATAR_FOLDER_PATH,
    FILE_SKIN_FOLDER_PATH,
    baseURL,
} = require('../../../config')

const initUploadAvatarAndSkin = ({ io, socket, sessionStore }) => {
    // 监听头像/皮肤上传 - 处理base64字符串
    socket.on('upload file', (file, type, kind, callback) => {
        // 获取base64图片信息
        // console.log(type) // base64  file
        // kind 种类 是 头像还是皮肤

        // 决定文件存储到那个文件夹下
        let FILEPATH = FILE_AVATAR_FOLDER
        // 决定文件存储到那个文件夹下完整路径
        let storePath = FILE_AVATAR_FOLDER_PATH
        if (kind === 'skin') {
            FILEPATH = FILE_SKIN_FOLDER
            storePath = FILE_SKIN_FOLDER_PATH
        }

        // 存储图片的文件夹
        if (!fs.existsSync(storePath)) {
            fs.mkdirSync(storePath)
        }

        let fileName
        let fileData
        if (type === 'base64') {
            // 图片名称和路径
            //过滤data:URL
            let base64Data = file.replace(/^data:image\/\w+;base64,/, '')
            fileData = new Buffer.from(base64Data, 'base64')

            // 为文件名增加一个随机数，防止同名文件覆盖
            fileName = uuidv4() + '.png' // ⇨ '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d'
        } else if (type === 'file') {
            // 获取base64图片信息
            let info = imageinfo(file)
            // 获取图片后缀
            let ext = null
            try {
                ext = path.basename(info.mimeType || 'png')
            } catch (e) {
                ext = 'jpg'
            }

            // 为文件名增加一个随机数，防止同名文件覆盖
            fileName = uuidv4() + '.' + ext // ⇨ '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d'
            fileData = file
        }
        // console.log('fileName...', fileName)
        const fullFileName = path.join(storePath, fileName)
        // console.log('fullFileName...', fullFileName)
        // 存储链接
        const url = `${baseURL}${FILEPATH}/${fileName}`
        // console.log(url)

        // 写入文件内容
        try {
            fs.writeFileSync(fullFileName, fileData)
            // 写入成功
            callback({ url })

            if (kind === 'avatar') {
                // 将修改后的头像地址，赋值给socket.avatar
                socket.avatar = url

                // 群发给所有用户
                io.emit('change userInfo', {
                    type: 'avatar',
                    userID: socket.userID,
                    avatar: url,
                })
            } else if (kind === 'skin') {
                socket.skin = url
                // 无需群发消息广播
            }

            // 重新存储到session中
            sessionStore.saveSession(socket.sessionID, {
                connected: true,
                userID: socket.userID,
                username: socket.username,
                avatar: socket.avatar,
                skin: socket.skin,
                isTeacher: socket.isTeacher,
                disabledChatMsg: socket.disabledChatMsg,
                time: moment().format(),
            })
        } catch (err) {
            // 写入失败
            callback('')
        }
    })
}

module.exports = initUploadAvatarAndSkin
