const initResetChatMessage = ({ io, socket, chatMessageStore, users }) => {
    // 重置群聊的消息
    socket.on('reset chat message', (callback) => {
        // 只能是老师来操作
        if (!socket.isTeacher) return

        // 只给userID为1的添加群聊消息，其他的用户无需挂载群聊消息
        let user = users.find((item) => item.userID === 1)
        // 群聊的消息
        user.messages = []
        chatMessageStore.splice(0)

        callback()

        // 通知所有人，重置所有的消息
        io.emit('teacher reset chat message')
    })
}

module.exports = initResetChatMessage
