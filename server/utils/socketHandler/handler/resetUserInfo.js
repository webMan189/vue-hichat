const moment = require('moment')

const { changeTopSkin } = require('../../changeSkin')
const { nameToStar } = require('../../nicknameToStar')

const initResetUserInfo = ({ io, socket, sessionStore, allSenNames }) => {
    // 更换用户信息（头像 type为avatar、皮肤 type为skin）
    socket.on(
        'change userInfo',
        async ({ type, url, getRemoteSkin }, callback) => {
            if (type === 'avatar') {
                // 将修改后的头像地址，赋值给socket.avatar
                socket.avatar = url

                // 群发给所有用户
                io.emit('change userInfo', {
                    type: 'avatar',
                    userID: socket.userID,
                    avatar: url,
                })
            } else if (type === 'skin') {
                if (url) {
                    // 有数据， 将修改后的皮肤地址，赋值给socket.skin
                    socket.skin = url
                } else {
                    try {
                        // 如果没有传递url，就是在随机更换皮肤
                        const res = await changeTopSkin(getRemoteSkin)

                        if (typeof res === 'string') {
                            socket.skin = res
                        } else if (typeof res === 'object') {
                            if (res.type === 'video') {
                                // socket.skin = res.src
                                socket.skin = res
                            } else if (res.type === 'canvas') {
                                // 暂不处理
                            }
                        }

                        // 将数据传递回去
                        callback(res)
                    } catch (error) {
                        console.log('随机图片失败', error)
                    }
                }

                // 无需群发给所有用户
                // io.emit('change userInfo', {
                //     userID: socket.userID,
                //     skin: url,
                // })
            } else if (type === 'name') {
                // 修改昵称
                // console.log('收到新的名字 ', url)

                // 查看新的名字是否已经使用了
                let res = sessionStore
                    .findAllSessions()
                    .find((item) => item.username === url)

                if (res) {
                    // 用户名相同
                    return callback('duplication username')
                }

                // 将修改后的昵称，赋值给socket.username
                // 昵称交给nameToStar函数去处理，如果包含敏感字眼，处理成**
                const resName = nameToStar(url, allSenNames)
                socket.username = resName

                // 群发给所有用户
                io.emit('change userInfo', {
                    type: 'name',
                    userID: socket.userID,
                    username: resName,
                })

                callback('is ok')
            }

            // 重新存储到session中
            sessionStore.saveSession(socket.sessionID, {
                connected: true,
                userID: socket.userID,
                username: socket.username,
                avatar: socket.avatar,
                skin: socket.skin,
                isTeacher: socket.isTeacher,
                disabledChatMsg: socket.disabledChatMsg,
                time: moment().format(),
            })
        }
    )
}

module.exports = initResetUserInfo
