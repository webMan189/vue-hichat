const fs = require('fs')
const path = require('path')
const { v4: uuidv4 } = require('uuid')
// 解决识别base64图片后缀问题
const imageinfo = require('imageinfo')

// 导入配置
const {
    baseURL,
    FILE_FOLDER_PATH: storePath,
    FILES_FOLDER,
} = require('../../../config')

const initUploadFiles = ({ io, socket }) => {
    // 监听文件上传
    socket.on('upload', async (file, callback) => {
        // console.log(file) // <Buffer 25 50 44 ...>
        // console.log(file.toString('base64'))

        // 获取base64图片信息
        let info = imageinfo(file)
        // console.log('Data is type:', info.mimeType)
        // 获取图片后缀
        let ext = null
        try {
            ext = path.basename(info.mimeType || 'png')
        } catch (e) {
            ext = 'jpg'
        }
        // console.log(ext)

        // console.log(storePath)
        // 存储图片的文件夹
        if (!fs.existsSync(storePath)) {
            fs.mkdirSync(storePath)
        }
        // 图片名称和路径
        // console.log(path.extname(file))

        // 为文件名增加一个随机数，防止同名文件覆盖
        const fileName = uuidv4() + '.' + ext // ⇨ '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d'
        // console.log('fileName...', fileName)

        const fullFileName = path.join(storePath, fileName)
        // console.log('fullFileName...', fullFileName)

        // 存储链接
        const url = `${baseURL}${FILES_FOLDER}/${fileName}`
        // console.log(url)

        // save the content to the disk, for example
        fs.writeFile(fullFileName, file, (err) => {
            // console.log({
            //     message: err ? 'failure' : 'success',
            //     data: { url, alt: fileName, href: url },
            // })
            callback({ url, alt: fileName, href: url })
        })
    })
}

module.exports = initUploadFiles
