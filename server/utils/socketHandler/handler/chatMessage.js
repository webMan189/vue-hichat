const moment = require('moment')
const { findMentionPersonHandler } = require('../../findMentionPerson')

const initChatMessage = ({ io, socket, chatMessageStore }) => {
    // 班级群聊
    socket.on('chat message', ({ content }) => {
        // console.log(content)

        const message = {
            from: socket.userID,
            content,
            userID: socket.userID,
            username: socket.username,
            avatar: socket.avatar,
            skin: socket.skin,
            isTeacher: socket.isTeacher,
            disabledChatMsg: socket.disabledChatMsg,
            connected: true,
            time: moment().format(),
            // 消息id，便于找到该id的消息
            // messageID: uuidv4(),
        }

        socket.broadcast.emit('chat message', message)

        // 有人@你
        findMentionPersonHandler(content).forEach((item) => {
            // console.log(item)
            socket.to(item).to(socket.userID).emit('someone @ you', message)
        })

        // 将群聊的消息存储到服务器端
        chatMessageStore.push(message)
    })
}

module.exports = initChatMessage
