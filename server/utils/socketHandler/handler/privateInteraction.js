const _ = require('lodash')

const initPrivateInteraction = ({ io, socket }) => {
    // 给某人发送炸弹、鲜花等
    socket.on('private interaction', ({ type, to }) => {
        // console.log('给某人发送炸弹、鲜花等 ', type, to)
        // type 类型
        // socket.to(to).emit() 发消息给给定的用户ID

        _.delay(
            () => {
                socket
                    .to(to)
                    .emit('private interaction', { from: socket.userID, type })
            },
            type === 'bomb' ? 1500 : 0
        )
    })
}

module.exports = initPrivateInteraction
