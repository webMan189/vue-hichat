const moment = require('moment')

const initPrivateMessage = ({ io, socket, messageStore }) => {
    // 私聊
    socket.on('private message', ({ content, to }) => {
        // socket.to(to).emit() 发消息给给定的用户ID
        const message = {
            content,
            from: socket.userID,
            to,
            username: socket.username,
            avatar: socket.avatar,
            isTeacher: socket.isTeacher,
            disabledChatMsg: socket.disabledChatMsg,
            time: moment().format(),
        }
        socket.to(to).to(socket.userID).emit('private message', message)
        // 将私聊的消息存储到服务器端
        messageStore.saveMessage(message)
    })
}

module.exports = initPrivateMessage
