const moment = require('moment')

const initDisconnect = ({ io, socket, sessionStore }) => {
    // 断线
    socket.on('disconnect', async () => {
        const matchingSockets = await io.in(socket.userID).allSockets()
        const isDisconnected = matchingSockets.size === 0
        if (isDisconnected) {
            // notify other users
            socket.broadcast.emit('user disconnected', socket.userID)
            // update the connection status of the session
            // console.log('离线的时候，socket.avatar ', socket.avatar)

            // 先找到该离线用户的最新信息（以前的代码）
            // const session = sessionStore.findSession(socket.sessionID)

            // 在把最新信息存储起来
            sessionStore.saveSession(socket.sessionID, {
                userID: socket.userID,
                username: socket.username,
                avatar: socket.avatar,
                skin: socket.skin,
                isTeacher: socket.isTeacher,
                disabledChatMsg: socket.disabledChatMsg,
                time: moment().format(),
                connected: false,
            })
        }
    })
}

module.exports = initDisconnect
