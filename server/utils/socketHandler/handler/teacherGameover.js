const moment = require('moment')

const initTeacherGameover = ({ io, socket, sessionStore }) => {
    // 老师 踢人
    socket.on('gameover', ({ id }, callback) => {
        // console.log('老师 要剔除的人id', id)

        // 如果不是老师的操作，则阻止
        if (!socket.isTeacher) return

        /*
            ! 思路梳理
            todo 1. 需要根据传过来的用户id来获取到sessionID
            2. 根据sessionID 重新存储数据
        */

        const { sessionID, user } = sessionStore.findKey(id)
        // console.log('---------------')
        // console.log(sessionID, user)

        // 重新存储到session中
        sessionStore.saveSession(sessionID, {
            ...user,
            connected: false,
            isTeacher: false,
            time: moment().format(),
        })
        // console.log('踢人后的所有sessions', sessionStore.findAllSessions())

        // 发消息给给定的用户ID
        // socket.to(id).emit('teacherSayGameOver')

        // 群发给其他用户
        io.emit('teacherSayGameOver', { id, username: user.username })

        callback(null)
    })
}

module.exports = initTeacherGameover
