const _ = require('lodash')
const moment = require('moment')

// 导入配置
const { avatarSheet, usernameSheet } = require('../../../config')

// 加载皮肤和头像
const avatars = require(avatarSheet)
const nickname = require(usernameSheet)

const initTeacherResetUser = ({ io, socket, sessionStore }) => {
    // 老师重置学生用户姓名和头像
    socket.on('resetUsrnameAndAvatar', ({ id }, callback) => {
        // console.log('老师重置学生用户姓名和头像', id)

        // 如果不是老师的操作，则阻止
        if (!socket.isTeacher) return

        // 根据id查找指定用户
        const { sessionID, user } = sessionStore.findKey(id)
        // console.log('查找到的用户', user)
        // console.log('当前操作用户id', socket.userID, socket.isTeacher)

        // 重置后的头像
        const avatar = avatars[0][0]
        // 重置后的头像（用户名是随机的）
        let username = _.sample(nickname)

        // 还需要检测用户名是否已被占用
        // 查看新的名字是否已经使用了
        let res = sessionStore
            .findAllSessions()
            .find((item) => item.username === username)

        if (res) {
            // 用户名相同 ==> 在继续随机一次就好了（如果还是重名的，也没事）
            username = _.sample(nickname)
        }

        // 修改指定用户的用户名和头像
        user.avatar = avatar
        user.username = username

        // 重新存储到session中
        sessionStore.saveSession(sessionID, {
            ...user,
            connected: true,
            isTeacher: false,
            time: moment().format(),
        })

        // 更新对应用户的socket信息为最新的
        // 所有用户的sockets
        // console.log(io.of('/').sockets)
        for (let [, value] of io.of('/').sockets.entries()) {
            if (value.userID === id) {
                // 用户的socket中的avatar和username信息
                value.avatar = avatar
                value.username = username

                // 找到了该用户并修改了信息，就可以退出循环了
                break
                // console.log(key, value)
            }
        }
        // console.log('-------------------')
        // console.log(io.of('/').sockets)

        // 群发给所有用户
        io.emit('change userInfo', {
            type: 'nameAndAvatar',
            avatar,
            username,
            userID: id,
        })

        // console.log('修改后的所有sessions', sessionStore.findAllSessions())

        // 执行回调函数，无错误
        callback(null)
    })
}

module.exports = initTeacherResetUser
