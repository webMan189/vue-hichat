const fs = require('fs')
const { v4: uuidv4 } = require('uuid')

// 导入配置
const { musicSheet: jsonPath } = require('../../../config')

const initMusic = ({ io, socket }) => {
    // 监听音乐-发送回去
    socket.on('music list', (callback) => {
        // 读取json文件

        const data = fs.readFileSync(jsonPath)
        // 执行回调函数，并将内容转字符串作为实参返回
        callback(data.toString())
    })

    // 监听音乐-录入到json文件中
    socket.on('add music', (name, callback) => {
        // json文件路径

        try {
            // 歌曲对象
            const res = {
                name,
                count: 100, // 最低数量为100
                id: uuidv4(),
            }
            // 读取json文件
            const data = fs.readFileSync(jsonPath).toString()
            // console.log(`data: ${data}`)
            const arr = JSON.parse(data)
            arr.unshift(res)
            // console.log(arr)

            fs.writeFileSync(jsonPath, JSON.stringify(arr))
            // 执行回调函数，并将内容转字符串作为实参返回
            // 参数1： 失败信息，为空，表示没有失败
            callback('', JSON.stringify(res))

            // 群发给所有用户, 新添加的歌曲
            io.emit('get add music', JSON.stringify(res))
        } catch (e) {
            // 参数1： 失败信息
            callback(e)
        }
    })

    // 监听音乐-点赞
    socket.on('like music', (id, callback) => {
        // json文件路径

        try {
            // 读取json文件
            const data = fs.readFileSync(jsonPath).toString()
            const arr = JSON.parse(data)
            const obj = arr.find((item) => item.id === id)
            obj.count++

            fs.writeFileSync(jsonPath, JSON.stringify(arr))
            callback('')

            // 群发给所有用户, 新点赞的歌曲
            io.emit('get like music', obj)
        } catch (e) {
            // 参数1： 失败信息
            callback(e)
        }
    })
}

module.exports = initMusic
