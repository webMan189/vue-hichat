const moment = require('moment')

// 导入配置
const { FILE_AVATAR_FOLDER, baseURL } = require('../../config')
const init = require('./init')

const ioConnection = ({
    io,
    sessionStore,
    users,
    messageStore,
    chatMessageStore,
    allSkins,
    allSenNames,
}) => {
    // ! Upon connection, we send all existing users to the client
    // 在连接时，我们将现在现有用户发送到客户机
    io.on('connection', (socket) => {
        // console.log('connection')
        // console.log(sessionStore.findAllSessions())

        sessionStore.saveSession(socket.sessionID, {
            userID: socket.userID,
            username: socket.username,
            avatar: socket.avatar,
            skin: socket.skin,
            // 决定是否为老师
            isTeacher: socket.isTeacher,
            disabledChatMsg: socket.disabledChatMsg,
            connected: true,
            time: moment().format(),
        })

        // emit session details
        socket.emit('session', {
            sessionID: socket.sessionID,
            userID: socket.userID,
            // 响应回去用户是否为老师
            isTeacher: socket.isTeacher,
        })

        // join the "userID" room
        socket.join(socket.userID)

        // 存储所有用户信息
        users = [
            // 当前群聊
            {
                userID: 1,
                username: '传智黑马前端大神班级群',
                avatar: `${baseURL}${FILE_AVATAR_FOLDER}/logo.png`,
                connected: true,
                messages: [],
            },
        ]

        // And we fetch the list of messages upon connection
        // 我们在连接时获取消息列表
        const messagesPerUser = new Map()
        // 私聊的消息
        messageStore.findMessagesForUser(socket.userID).forEach((message) => {
            const { from, to } = message
            const otherUser = socket.userID === from ? to : from
            if (messagesPerUser.has(otherUser)) {
                messagesPerUser.get(otherUser).push(message)
            } else {
                messagesPerUser.set(otherUser, [message])
            }
        })

        sessionStore.findAllSessions().forEach((session) => {
            users.push({
                userID: session.userID,
                username: session.username,
                avatar: session.avatar,
                connected: session.connected,
                skin: session.skin,
                isTeacher: session.isTeacher,
                disabledChatMsg: session.disabledChatMsg,
                time: moment().format(),
                // 私聊消息
                messages: messagesPerUser.get(session.userID) || [],
            })

            // 只给userID为1的添加群聊消息，其他的用户无需挂载群聊消息
            let user = users.find((item) => item.userID === 1)
            // 群聊的消息
            user.messages = chatMessageStore
        })
        // console.log('findAllSessions', sessionStore.findAllSessions())

        // 初始化 socket
        init({
            io,
            socket,
            sessionStore,
            chatMessageStore,
            messageStore,
            allSenNames,
            users,
            allSkins,
        })
    })
}

module.exports = ioConnection
