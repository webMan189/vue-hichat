// 正则： span标签 + data-mention自定义属性 + data-user-id自定义属性 + 内容
const reg =
    /<(span class="mention" data-mention="@\S+" data-user-id="(\S+)")[^>]*>(.*?<\/\1>)?/gm

// 找所有的@的id
const findMentionPersonHandler = (content) => {
    let arr = []

    while (true) {
        let res = reg.exec(content)
        if (res) {
            res = res[2]
            arr.push(res)
        } else {
            break
        }
        // console.log(res)
    }

    // 将数组进行去重后返回
    return [...new Set(arr)]
}

module.exports = { findMentionPersonHandler }

// 测试代码
// let str = `
//             <p>全微分<span class="mention" data-mention="@杨怡" data-user-id="3242321321321321">@杨怡</span> 青山区<br>&nbsp;</p>
//             <p>去我<span class="mention" data-mention="@千玺" data-user-id="1444c5e1d9ef679e">@千玺</span>去得分仍<br>&nbsp;</p>
//             <p>去我<span class="mention" data-mention="@千玺" data-user-id="1444c5e1d9ef679e">@千玺</span>去得分仍<br>&nbsp;</p>
//             <p>去我<span class="mention" data-mention="@千玺" data-user-id="123">@王五</span>去得分仍<br>&nbsp;</p>
//             <p>去我<span >王五王五王五</span>去得分仍<br>&nbsp;</p>
//             <p>去我qwer dwqef </p>
//         `
// console.log(findMentionPersonHandler(str))
