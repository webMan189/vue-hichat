// 处理默认提供的皮肤 - 对外提供现成的完整路径

// 服务器的跟地址
const { FILE_SKIN_FOLDER, baseURL } = require('../config')

// 波仔图片
const bz1 = '/bg1.jpg'
const bz2 = '/bg2.jpg'
const bz3 = '/bg3.jpg'
const bz4 = '/bg4.jpg'
const bz5 = '/bg5.jpg'

// 其他
const bg = '/bg.jpg'
const bg1 = '/1.png'
const bg2 = '/2.jpg'
const bg3 = '/3.jpg'
const bg4 = '/4.jpg'
const bg5 = '/5.jpg'
const bg6 = '/6.jpg'
const bg7 = '/7.png'
const bg8 = '/8.jpg'
const bg9 = '/9.png'
const bg10 = '/10.jpg'
const bg11 = '/11.jpg'
const bg12 = '/12.jpg'
const bg13 = '/13.jpg'
const bg14 = '/14.jpg'
const bg15 = '/15.jpg'
const bg16 = '/16.jpg'
const bg17 = '/17.jpg'
const bg18 = '/18.jpg'
const bg19 = '/19.jpg'
const bg20 = '/20.jpg'
const bg21 = '/21.jpg'
const bg22 = '/22.jfif'
const bg23 = '/23.webp'
const bg24 = '/24.jpg'
const bg25 = '/25.jpg'
const bg26 = '/26.jpg'

// 16个皮肤
// let arr = [
//     [bz1, bz2, bz3, bz4],
//     [bz5, bg13, bg1, bg2],
//     [bg4, bg5, bg8, bg3],
//     [bg9, bg10, bg12, bg14],
// ]

// 返回九个即可
// let arr = [
//     [bz1, bz2, bz3],
//     [bz4, bz5, bg13],
//     [bg1, bg4, bg10],
// ]

// 前端修改为前2个分别展示上传组件 + 随机切换壁纸，所以九宫格数据（只需要7个数据即可）
let arr = [
    [, , bz3],
    [bz2, bz1, bz4],
    [bz5, bg13, bg1],
    [bg12, bg14, bg10],
]

// 可以随机的其他皮肤
let randomSkins = [
    bg,
    bg2,
    bg3,
    bg5,
    bg6,
    bg7,
    bg8,
    bg9,
    bg11,
    bg12,
    bg14,
    bg15,
    bg16,
    bg17,
    bg18,
    bg19,
    bg20,
    bg21,
    bg22,
    bg23,
    bg24,
    bg25,
    bg26,
]

// 处理好路径
const skins = arr.map((secondArr) =>
    secondArr.map((item) => baseURL + FILE_SKIN_FOLDER + item)
)

// 所有皮肤扁平化处理
const allSkins = skins.flat(1)

// 处理好路径
randomSkins = randomSkins.map((item) => baseURL + FILE_SKIN_FOLDER + item)
// console.log(randomSkins)

module.exports = { skins, allSkins, randomSkins }
