const axios = require('axios')
const _ = require('lodash')

// 导入配置文件
const { skinRequestUrl, skinSheet } = require('../config')

// 导入皮肤
const { randomSkins } = require(skinSheet)

// 获取6页热门壁纸，用于切换
;(async function () {
    for (let i = 1; i <= 6; i++) {
        await requestTopSkin(i)
    }
    // 获取视频壁纸
    await videoSkin()
    // console.log('随机的数组长度 ')
    // console.log(JSON.stringify(randomSkins, '', 2))
    // console.log('随机的数组长度 ', randomSkins.length)
})()

// 随机切换热门壁纸函数，如果壁纸切换完毕了，当切换的次数达到了数组的个数，就加载后5页数据
async function changeTopSkin() {
    let skin = _.sample(randomSkins)
    return skin
}

// 获取热门壁纸
async function requestTopSkin(page = 1) {
    try {
        const {
            data: { code, data, msg, thumb },
        } = await axios.get(skinRequestUrl.top, {
            params: {
                page,
            },
            // `timeout` 指定请求超时的毫秒数。
            // 如果请求时间超过 `timeout` 的值，则请求会被中断
            timeout: 5000, // 默认值是 `0` (永不超时)
        })
        if (code !== 200) {
            // 失败
        } else {
            // 添加到randomSkins数组中
            data.forEach((item) => randomSkins.push(item.raw))
            // console.log('数组的长度', randomSkins.length)
        }
    } catch (e) {
        console.log(`获取热门壁纸失败: ${e.message}`)
    }
}

// 随机切换皮肤函数
const randomSkinHandler = async (remoteSkin = true) => {
    // 需要返回的结果
    let res = null

    // console.log(randomSkins)

    if (!remoteSkin) {
        // 如果不是要远程随机的图片，就从本地服务器随机一个
        // 以备不时之需（图片服务器挂掉了）
        res = _.sample(randomSkins)
    } else {
        try {
            const {
                data: { code, data, msg, thumb },
            } = await axios.get(skinRequestUrl.random, {
                // `timeout` 指定请求超时的毫秒数。
                // 如果请求时间超过 `timeout` 的值，则请求会被中断
                timeout: 3000, // 默认值是 `0` (永不超时)
            })
            if (code !== 200) {
                // 失败
                res = _.sample(randomSkins)
            } else {
                res = data
            }
        } catch {
            res = _.sample(randomSkins)
        }
    }

    return res
}

// 获取视频壁纸
async function videoSkin(page = 1) {
    try {
        const {
            data: { code, data, message },
        } = await axios.get(skinRequestUrl.video, {
            params: {
                page,
            },
            // `timeout` 指定请求超时的毫秒数。
            // 如果请求时间超过 `timeout` 的值，则请求会被中断
            timeout: 5000, // 默认值是 `0` (永不超时)
        })
        if (code !== 0) {
            // 失败
        } else {
            // 添加到randomSkins数组中
            data?.list?.forEach((item) =>
                randomSkins.push({ src: item.src, type: 'video' })
            )
            // console.log('数组的长度', randomSkins.length)
        }
    } catch (e) {
        console.log(`获取视频壁纸失败: ${e.message}`)
    }
}

module.exports = {
    randomSkinHandler,
    changeTopSkin,
}

// todo 测试
// setTimeout(() => {
//     for (let i = 1; i <= 25; i++) {
//         ;(async function () {
//             console.log(await changeTopSkin())
//         })()
//     }
// }, 5000)

// 测试
// for (let i = 1; i <= 1; i++) {
//     ;(async function () {
//         // await requestTopSkin(1)
//         // await requestTopSkin(2)
//         // await requestTopSkin(3)
//         // await requestTopSkin(4)
//         // await requestTopSkin(5)
//         const res2 = await requestSkin()
//         console.log(res2)
//         // const res = await requestSkin(false)
//         // console.log(res)
//     })()
// }
