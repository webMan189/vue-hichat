const os = require('os')

class OSUtil {
    static getYitai() {
        let os = require('os'),
            iptable = {},
            ifaces = os.networkInterfaces(),
            res = OSUtil.getIPV4Address()

        for (var dev in ifaces) {
            ifaces[dev].forEach(function (details, alias) {
                if (details.family == 'IPv4') {
                    // console.log(dev)
                    iptable[dev + (alias ? ':' + alias : '')] = details.address

                    if (dev.includes('以太网')) {
                        console.log(`以太网: ${details.address}`)
                        res = details.address
                    }
                }
            })
        }

        return res
    }

    /**
     * 获取系统ipv4 地址
     * @return {string | (() => AddressInfo) | (() => (AddressInfo | {})) | (() => (AddressInfo | string | null))}
     */
    static getIPV4Address() {
        const interfaces = os.networkInterfaces()
        for (const devName in interfaces) {
            //
            //console.log(devName)
            const iface = interfaces[devName]
            //console.log(iface)
            for (let i = 0; i < iface.length; i++) {
                let alias = iface[i]
                //console.log(alias)
                if (
                    alias.family === 'IPv4' &&
                    alias.address !== '127.0.0.1' &&
                    !alias.internal
                ) {
                    return alias.address
                }
            }
        }
        return null
    }

    /**
     * 获取系统的ipv6地址
     * @return {string|(() => AddressInfo)|(() => (AddressInfo | {}))|(() => (AddressInfo | string | null))|null}
     */
    static getIPV6Address() {
        const interfaces = os.networkInterfaces()
        for (const devName in interfaces) {
            //
            const iface = interfaces[devName]
            for (let i = 0; i < iface.length; i++) {
                let alias = iface[i]
                //console.log(alias)
                if (
                    alias.family === 'IPv6' &&
                    alias.address !== '::1' &&
                    !alias.internal
                ) {
                    return alias.address
                }
            }
        }
        return null
    }
}

module.exports = OSUtil
