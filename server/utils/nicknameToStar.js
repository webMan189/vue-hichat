const fs = require('fs')
const { namesSensitive } = require('../config')

// 读取文件内容
function readNames() {
    let res
    try {
        res = JSON.parse(fs.readFileSync(namesSensitive).toString())
    } catch {
        res = []
    }
    return res
}

// 处理敏感昵称为**
function nameToStar(uname, arr) {
    const res = arr.find((item) => uname.includes(item))
    const reg = new RegExp(res, 'g')

    if (res) {
        return uname.replace(reg, () => '*'.repeat(res.length))
    }
    // 原样输出
    return uname
}

module.exports = { readNames, nameToStar }
