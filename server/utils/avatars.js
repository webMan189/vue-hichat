// 服务器的跟地址
const { FILE_AVATAR_FOLDER, baseURL } = require('../config')

// 处理默认提供的头像 - 对外提供现成的完整路径
const avatar1 = '/1.jpeg'
const bz1 = '/1.png'
const bz2 = '/2.png'
const bz3 = '/3.png'
const bz4 = '/4.png'
const vue = '/vue.webp'
const react = '/react.webp'
const cat = '/cat.jpeg'
const avatar4 = '/4.webp'
const avatar2 = '/2.webp'
const avatar3 = '/3.webp'
const avatar5 = '/5.webp'
const avatar6 = '/6.webp'
const avatar7 = '/7.webp'
const avatar8 = '/8.webp'
const avatar9 = '/9.webp'
const avatar10 = '/10.webp'
const avatar11 = '/11.webp'
const avatar12 = '/12.webp'
const avatar13 = '/13.webp'

const arr = [
    [bz3, bz2, bz1, bz4],
    [avatar4, avatar2, avatar3, avatar5],
    [avatar6, avatar7, avatar8, avatar9],
    [vue, avatar11, avatar12, avatar13],
]

const avatarArr = arr.map((secondArr) =>
    secondArr.map((item) => baseURL + FILE_AVATAR_FOLDER + item)
)

// console.log(avatarArr)

module.exports = avatarArr
