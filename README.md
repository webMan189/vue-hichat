# hichat 聊天室

## 说明

> 这里的版本是试验最新功能的，简化代码，以便可以最终上线的效果代码

## 使用步骤

1. 需要在教室内使用的话，需要下载源码，并将项目在教室局域网环境内（插入教室网线）的情况下 执行 `yarn build` 打包项目，生成 `dist`
2. 在将 `dist` 目录丢到 服务器环境中
3. 之后需要启动 `server` 目录中的 `app-test.js` 文件了

    ```bash
     node app-test.js

     # or
     yarn test

     # 说明： 在启动服务器成功之后，会出现老师登录的验证码哦，用于老师登录使用
    ```

4. 分享教室局域网的 ip 地址给学生。就可以局域网聊天，点名了
5. 老师登录
    1. 用户名必须是 `admin`，不论大小写
    2. 会自动出现验证码输入框，验证码在服务器的终端中会展示出来（小心学生看到验证码，并被学生抢先注册为老师身份哦）
    3. 登录到系统之后，老师的 `admin` 用户名会自动展示为 `黑马老师`（也可以之后编辑用户名）
6. 学生登录
    1. 自己填写用户名或者随机用户名，就可以直接登录，无需密码的

## 效果预览

### 登录页

![image-20230322215217032](README.assets/image-20230322215217032.png)

### 后台主页

![image-20230322215232071](README.assets/image-20230322215232071.png)

### 聊天功能

![image-20230322215556256](README.assets/image-20230322215556256.png)

### 图片预览功能

![image-20230322215718103](README.assets/image-20230322215718103.png)

### 随机点名功能

![image-20230322215749513](README.assets/image-20230322215749513.png)

### 换肤功能

![image-20230322215247148](README.assets/image-20230322215247148.png)

### 更换昵称功能

![image-20230322215259355](README.assets/image-20230322215259355.png)

### 更换头像功能

![image-20230322215313945](README.assets/image-20230322215313945.png)

### 头像裁切上传功能

![image-20230322215327897](README.assets/image-20230322215327897.png)

## 功能列表

1. 登录功能
2. 登录状态不丢失（包含头像，昵称，皮肤数据持久存储）
3. 修改昵称
4. 更换 + 裁切 + 上传头像功能
5. 群聊功能
6. 私聊功能
7. 更换皮肤
8. 全屏
9. 发送图片
10. 发送表情包
11. 图片放大预览效果

## hichat client 客户端

```bash
yarn
```

### Compiles and hot-reloads for development

```bash
yarn serve
```

### Compiles and minifies for production

```bash
yarn build
```

## hichat server 服务端

### Project setup

```bash
cd server

yarn
```

### Compiles and hot-reloads for development

```bash
node app-test.js

# or
yarn test
```
